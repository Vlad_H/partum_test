
# Project Title

Test task for Partum



## Pre-conditions
Install dependencies using this command:
```bash
yarn
```

## Running Tests

To open Cypress App, run the following command:
```bash
  yarn cypress:open
```

To run all specs in headless mode , run the following command

```bash
  yarn cypress:headless
```

To run a single spec, run the following command
```bash
yarn cypress run --spec <spec_name>
```

## Generate Report
To generate Mochawesome report, run the following command
```bash
yarn mochawesome:generate
```

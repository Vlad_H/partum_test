import {MainPage} from "./main.page";

export class Toolbar extends  MainPage{

    get burgerMenuButton(){
        return super.toolbar().find('button.v-app-bar__nav-icon')
    }

    get notificationButton() {
        return super.toolbar().xpath('//*[@id="app"]/div/header/div/div/div[2]/button[1]')
    }

    get userMenu() {
        return super.toolbar().find('button.user-menu')
    }

    get profileMenuOption() {
        return cy.get('#list-item-275')
    }

    get changePasswordMenuOption() {
        return cy.get('#list-item-277')
    }

    get logoutMenuOption() {
        return cy.get('#list-item-280')
    }

}
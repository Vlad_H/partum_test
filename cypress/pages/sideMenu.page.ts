import {MainPage} from "./main.page";

export class SideMenuPage extends  MainPage{

    get chatsTab(){
        return super.sideMenu().find('a[href="/chats"]')
    }

    get companiesTab(){
        return super.sideMenu().xpath('//*[@id="main-sidebar"]/div[1]/div/div[1]/div[3]/div[1]/div')
    }

    get facilitiesTab(){
        return super.sideMenu().find('a[href="/company/facilities"]')
    }

    get usersTab(){
        return super.sideMenu().xpath('//*[@id="main-sidebar"]/div[1]/div/div[1]/div[3]/div[2]/div[1]')
    }

    get driversTab(){
        return super.sideMenu().find('a[href="/users/drivers"]')
    }

    get ownersTab(){
        return super.sideMenu().find('a[href="/users/owners"]')
    }

    get coordinatorsTab(){
        return super.sideMenu().find('a[href="/users/coordinators"]')
    }

    get fleetsTab(){
        return super.sideMenu().find('a[href="/users/coordinators"]')
    }


}
 export class LoginPage {
    static get email(){
        return cy.get('input[name="email"]');
    }
    static get password(){
        return cy.get('input[name="password"]');
    }

     static get showPasswordButton(){
         return cy.get('button[aria-label="append icon"]');
     }

    static get submitButton(){
        return cy.get('button').contains('Log in');
    }

    static get forgotPasswordLink() {
        return cy.get('a[href="/reset-password"]');
    }

    static get invalidCredentialsError() {
        return cy.contains('Wrong Email or password')
    }

     static checkLoginPageElements() {
         this.email.should('be.visible')
         this.password.should('be.visible')
         this.forgotPasswordLink.should('be.visible')
         this.submitButton.should('be.visible')
     }

    static login(email: string, password: string) {
        this.email.realType(email)
        this.password.realType(password)
        this.submitButton.realClick()
    }
}
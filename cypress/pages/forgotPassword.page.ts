export class ForgotPasswordPage {
    static get email(){
        return cy.get('input[name="email"]');
    }

    static get submitButton(){
        return cy.get('button').contains('Send link to email');
    }

    static get backToLoginLink() {
        return cy.get('a[href="/login"]');
    }

    static checkForgotPasswordPageElements() {
        this.email.should('be.visible')
        this.submitButton.should('be.visible')
        this.backToLoginLink.should('be.visible')
    }

}
export class MainPage {
    public toolbar(){
        return cy.get('.v-toolbar__content');
    }

    public sideMenu(){
        return cy.get('.v-navigation-drawer__content')
    }

    public pageBody(){
        return cy.get('.v-main__wrap')
    }

}
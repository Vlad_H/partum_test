import {MainPage} from "./main.page";
import {address, location, name} from "../constants/facility.constants";

export class FacilitiesPage extends  MainPage{

    get addFacilityButton() {
        return super.pageBody().find('a[href="/company/facilities/new/edit"]')
    }

    get facilityTable() {
        return super.pageBody().find('table')
    }

    get nameInput() {
        return super.pageBody().find('input[name="name"]')
    }

    get addressInput() {
        return super.pageBody().find('input[name="address_line"]')
    }

    get locationInput() {
        return super.pageBody().find('.v-select__slot').find('input').first()
    }

    get saveButton() {
        return super.pageBody().find('button').contains('Save')
    }

    get requiredError() {
        return super.pageBody().find('.v-messages__message')
    }

     checkCreateFacilityPageElements() {
        this.nameInput.should('be.visible')
        this.addressInput.should('be.visible')
        this.locationInput.should('be.visible')
        this.saveButton.should('be.visible')
    }

    createNewFacility() {
        this.nameInput.type(name);
        this.addressInput.type(address);
        this.locationInput.type(location)
        cy.contains('NoDa').click()
        this.saveButton.click()
    }


}
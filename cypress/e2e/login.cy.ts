import {LoginPage} from "../pages/login.page";
import { faker } from '@faker-js/faker';
import {ForgotPasswordPage} from "../pages/forgotPassword.page";


describe('Login page', () => {

  before(() => {
    cy.visit('/')
    cy.clearAllStorages()
    cy.reload()
  })


  it('should open Login Page and have all required fields', () => {
    LoginPage.checkLoginPageElements()
  })

  it('should redirect user to the Forgot Password page by clicking on Forgot Password link',  ()=> {
    LoginPage.forgotPasswordLink.realClick()
    ForgotPasswordPage.checkForgotPasswordPageElements()
    ForgotPasswordPage.backToLoginLink.realClick()
  });

  it('should show/hide password by clicking on "eye" button',  ()=> {
    LoginPage.password.type(faker.internet.password(8))
    LoginPage.showPasswordButton.realClick()
        .invoke('attr', 'class')
        .should('contain', 'mdi-eye-outline')
    LoginPage.showPasswordButton.realClick()
        .invoke('attr', 'class')
        .should('contain', 'mdi-eye-off-outline')
  });

  it('should show "Wrong Email or password" error with empty Email and Password field', function () {
    LoginPage.password.clear()
    LoginPage.submitButton.realClick()
    LoginPage.invalidCredentialsError.should('be.visible')
  });

  it('should show "Wrong Email or password" error on invalid Email data ', function () {
    LoginPage.login(faker.internet.email(), Cypress.env('PASSWORD_DATA'))
    LoginPage.invalidCredentialsError.should('be.visible')
  });

  it('should show "Wrong Email or password" error on invalid Password data ', function () {
    LoginPage.login(Cypress.env('EMAIL_DATA'), faker.internet.password())
    LoginPage.invalidCredentialsError.should('be.visible')
  });

  it('should successfully authenticate user with valid Login and Password data',  ()=> {
    cy.intercept('POST', 'https://dev.omni-dispatch.com/api/v1/sign-in').as('createToken')
    cy.validLogin()
    cy.wait('@createToken')
  });

})
import {LoginPage} from "../pages/login.page";
import {SideMenuPage} from "../pages/sideMenu.page";
import {FacilitiesPage} from "../pages/facilities.page";
import {address, location, name} from "../constants/facility.constants";


describe('Positive case', () => {

    before(() => {
        cy.visit('/')
        cy.clearAllStorages()
        cy.reload()
    })


    it('should open Login Page and have all required fields', () => {
        LoginPage.checkLoginPageElements()
    })

    it('should successfully authenticate user with valid Login and Password data', () => {
        cy.intercept('POST', 'https://dev.omni-dispatch.com/api/v1/sign-in').as('createToken')
        cy.validLogin()
        cy.wait('@createToken')
    });

    it('should open Create Facility page', () => {
        const sidebar = new SideMenuPage()
        const facilitiesPage = new FacilitiesPage()
        sidebar.companiesTab.realClick()
        sidebar.facilitiesTab.click()
        facilitiesPage.addFacilityButton.realClick()
        facilitiesPage.checkCreateFacilityPageElements()
    });

    it('should create new facility ', () => {
        const facilitiesPage = new FacilitiesPage()
        facilitiesPage.createNewFacility()
        facilitiesPage.facilityTable.contains(name).should('be.visible')
    })
})
import * as dotenv from 'dotenv'
import {LoginPage} from "../pages/login.page";
dotenv.config()

Cypress.Commands.add('validLogin', () => {
        LoginPage.email.type(Cypress.env('EMAIL_DATA'))
        LoginPage.password.type(Cypress.env('PASSWORD_DATA'))
        LoginPage.submitButton.realClick()
})

Cypress.Commands.add('clearAllStorages', () => {
        cy.clearAllSessionStorage()
        cy.clearAllLocalStorage()
        cy.clearAllCookies()
})
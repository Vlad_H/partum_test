export {};

declare global {
    namespace Cypress {
        interface Chainable {
            validLogin(): void
            clearAllStorages(): void
        }
    }
}
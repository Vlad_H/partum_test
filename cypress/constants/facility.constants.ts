import {faker} from "@faker-js/faker";

export const name = faker.name.jobTitle()
export const address = faker.address.streetAddress()
export const location = 'NoDa'
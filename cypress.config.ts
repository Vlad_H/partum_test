import { defineConfig } from 'cypress'

export default defineConfig({
  projectId: 'pmq3nb',
  reporter: 'node_modules/mochawesome',
  reporterOptions: {
    reportDir: 'cypress/results',
    overwrite: false,
    html: false,
    json: true,
  },
  e2e: {
    baseUrl: 'https://dev.omni-dispatch.com',
    testIsolation: false,
  },
})